<?php

/**
 *  AJAX Responder for simple-md-wiki
 *  Copyright (C) 2016-2017  Brooks Kline <brookskline@caiyos.com>
 *
 *  This file is part of simple-md-wiki.
 *
 *  Simple-md-wiki is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Simple-md-wiki is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


// Process incoming POST data

// populate method and response object
$siteId = (isset($_POST["siteId"])) ? $_POST["siteId"] : null;
$method = (isset($_POST["method"])) ? $_POST["method"] : null;


// Load settings

// creates a 'siteObject' container will settings and methods
require_once("./Settings.php");


// Populate default response array

$response = [
    "status" => 2,  // i.e. no result
    "data" => null
];


// Deal with missing params

if (!$siteId || !$method) {
    $response["data"] = "ERROR: Missing parameters; exiting. (siteId=$siteId, method=$method)";
    echo json_encode($response);
    exit;
}


// Set some vars

$currentSite = $siteObject->getSite();
$outputFolder = $siteObject->getOutputFolder();


/**
 * Main Action Tree
 *
 */
switch($method) {

    case "Search":

        $searchString = $_POST["searchText"];

        // build exclude dirs
        $excludeDirString = "";
        $searchExcludes = $siteObject->getSearchExcludes();
        foreach ($searchExcludes["dirs"] as $dir) {
            $excludeDirString .= " --exclude-dir='$dir'";
        }

        // build excludes
        $excludeString = "";
        foreach ($searchExcludes["files"] as $mask) {
            $excludeString .= " --exclude='$mask'";
        }

        // run command
        $command = "cd " . $siteObject->getWebRoot() . "/$outputFolder && grep -r -n -i --include='*" . $siteObject->getSearchMask() . "' $excludeDirString $excludeString '$searchString'";

        $grepResult = shell_exec($command);
        if ($grepResult) {
            // build result array
            $resultArray = array();
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $grepResult) as $result) {
                if ($result) {

                    $filePath = substr($result, 0, strpos($result, ":"));
                    $filePath = str_replace($siteObject->getSearchMask(), "", $filePath);

                    $result = substr($result, strpos($result, ":") + 1);
                    $line = substr($result, 0, strpos($result, ":"));

                    $matchData = substr($result, strpos($result, ":") + 1);

                    // remove internal links
                    $matchData = preg_replace("/href=(\"|')#/", "", $matchData);
                    // clean match data
                    $matchData = trim($matchData);
                    //  * anchors
                    $matchData = preg_replace("/<\/?a\s?[^>]*>/", "", $matchData);
                    //  * all other tags (fix broken html)
                    $matchData = preg_replace("/<\/?[^>]*>/", "", $matchData);
                    //  * html comments
                    $matchData = preg_replace("/<\/?\!\-\-.*$/", "", $matchData);

                    // check for cleaned results without original string
                    if (stripos($matchData, $searchString) === false) {
                        continue;
                    }

                    // check for empty result
                    if (empty(trim($matchData))) {
                        continue;
                    }

                    // highlight search string
                    $matchData = preg_replace("/($searchString)/i", $siteObject->getTemplate("Search_Highlight"), $matchData);

                    // add results to array
                    if (array_key_exists($filePath, $resultArray)) {
                        //array_push($resultArray["$filePath"], $matchData);
                        $resultArray["$filePath"][] = [ $line, $matchData ];
                    }
                    else {
                        $resultArray["$filePath"] = [ [ $line, $matchData ] ];
                    }

                }

            }

            // process result array
            $resultsHtml = "";
            foreach ($resultArray as $filePath => $results) {
                $resultsHtml .= sprintf($siteObject->getTemplate("Search_Results_Start"), "/$filePath", $filePath);
                foreach ($results as $resultInstance) {
                    $resultsHtml .= sprintf($siteObject->getTemplate("Search_Result_Entry"), $resultInstance[0], $resultInstance[1]);
                }
                $resultsHtml .= sprintf($siteObject->getTemplate("Search_Results_End"), $siteObject->getTemplate("Top_Link"));
            }

            // populate response
            $response["status"] = 1;
            $response["data"] = $resultsHtml;

        }
        else {
            $response["data"] = sprintf($siteObject->getTemplate("Search_Results_None"), $searchString);
        }

        break;


    case "Publish":

        // clean existing
        $result = shell_exec($siteObject->getPublishCleanCommand());

        // build exclude array
        $excludes = "";
        $publishExcludes = $siteObject->getPublishExcludes();

        if (!empty($publishExcludes)) {
            foreach ($publishExcludes as $glob) {
                $excludes .= " -not -path '*/$glob/*' ";
            }
        }

        // get file list
        $filesToPublish = shell_exec("cd " . $currentSite["sourceRoot"] . " && find -name '*.md' $excludes");

        // process files, build sitetree
        $sitetree = null;
        $sitetreeArray = [];

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $filesToPublish) as $file) {

            if ($file) {
                // get file names and paths:
                $paths = $siteObject->getPaths($file);
                $breadcrumbs = sprintf($siteObject->getTemplate("Breadcrumbs"), $paths["sectionId"], $paths["sectionName"] . "/");
                $sourceFile = $currentSite["sourceRoot"] . $paths["href"] . $paths["baseName"] . ".md";
                $publishPath = $siteObject->getWebRoot() . "/" . $outputFolder . $paths["href"];
                $outputFile = $publishPath . "/" . $paths["baseName"] . ".php";

                // check for path
                if (!is_dir($publishPath)) {
                    mkdir($publishPath, 0775, true);
                }

                // publish file
                $result = shell_exec($siteObject->getPublishCommand() . " -V breadcrumbs='$breadcrumbs' '$sourceFile' > '$outputFile'");

                // populate sitetree array
                if ($result === null) {
                    $sitetreeArray[$paths["sectionName"]][$paths["baseName"]] = $paths["href"] . $paths["baseName"];
                }

            }

        }

        // Process sitetree array

        // create container
        if (array_filter($sitetreeArray)) {
            $sitetree = "<div id='Sitetree'>\n";
        }

        // sort main array
        ksort($sitetreeArray);

        // init toc
        $sitetreeToc = $siteObject->getTemplate("Sitetree_TOC_Start");

        // iterate sitetree array
        foreach ($sitetreeArray as $sectionName => $sectionArray) {

            // sort section
            ksort($sectionArray);

            // set header title and section start
            $sitetreeTitle = sprintf($siteObject->getTemplate("Sitetree_Header"), $siteObject->getSectionId($sectionName), $sectionName);
            $sitetree .= sprintf($siteObject->getTemplate("Sitetree_Section_Start"), $sitetreeTitle);

            // process entries
            foreach ($sectionArray as $name => $link) {
                $sitetree .= sprintf($siteObject->getTemplate("Sitetree_Entry"), $link, $name);
            }
            $sitetree .= sprintf($siteObject->getTemplate("Sitetree_Section_End"), $siteObject->getTemplate("Top_Link"));

            // add toc entry
            $sitetreeToc .= sprintf($siteObject->getTemplate("Sitetree_TOC_Entry"), $siteObject->getSectionId($sectionName), $sectionName);

        }

        // populate response
        if ($sitetree) {

            // close container, toc, append toc
            $sitetree .= "</div>";
            $sitetreeToc .= $siteObject->getTemplate("Sitetree_TOC_End") . "\n" . $siteObject->getTemplate("Top_Link");
            $sitetree = "$sitetreeToc\n$sitetree";

            $response["status"] = 1;
            $response["data"] = $sitetree;

            // write sitetree file
            $filePointer = fopen($siteObject->getWebRoot() . "/$outputFolder/Sitetree.php", "w");
            fwrite($filePointer, $sitetree);
            fclose($filePointer);

        }
        else {
            $response["data"] = $strings["Publish_Error"] . "<br/>$result";
        }

        break;

    case null:
        break;

}

// return json-encoded response
echo json_encode($response);
