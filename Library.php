<?php

/**
 *  A helper library for simple-md-wiki
 *  Copyright (C) 2016-2017  Brooks Kline <brookskline@caiyos.com>
 *
 *  This file is part of simple-md-wiki.
 *
 *  Simple-md-wiki is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Simple-md-wiki is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


class CaiyosLib {

    public function __construct($siteId, $webRoot)
    {
        if (! empty($siteId) && ! empty($webRoot) ) {
            $this->siteId = $siteId;
            $this->webRoot = $webRoot;
        }
        else {
            die("ERROR: No site id or webroot (or both).");
        }
    }

    private $siteId = null;

    private $webRoot = null;

    private $outputFolder = "Output-%s";

    // parameters

    private $searchParams = [
        "fileMask" => ".php",
        "excludes" => [
            "files" => [],
            "dirs" => [],
        ],
    ];

    private $publishParams = [
        "excludes" => [],
        "command" => "pandoc \
            -f markdown \
            -w html5 \
            --highlight-style=zenburn \
            --template Pandoc_Template \
            --toc \
            --toc-depth=2 \
            -T '%s' \
        ",
        "cleanCommand" => "rm -r /%s/*",
    ];


    /**
     * Current site settings
     *
     * Structure:
     * "sourceRoot" => "",
     * "title" => "",
     * "header" => "",
     * "branding" => "",
     */
    private $site = [];


    /**
     * Global strings
     *
     */
    private $strings = [];


    /**
     * Global templates
     *
     */
    private $templates = [];


    /**
     * Set global strings
     *
     */
    public function setStrings($strings)
    {
        if (is_array($strings)) {
            $this->strings = $strings;
        }
        else {
            die("ERROR: Not a valid strings array.");
        }
    }


    /**
     * Get global string
     *
     */
    public function getString($name)
    {
        return $this->strings[$name];
    }


    /**
     * Set global templates
     *
     */
    public function setTemplates($templates)
    {
        if (is_array($templates)) {
            $this->templates = $templates;
        }
        else {
            die("ERROR: Not a valid template array.");
        }
    }


    /**
     * Get global template
     *
     */
    public function getTemplate($name)
    {
        return $this->templates[$name];
    }


    /**
     * Set site settings
     *
     * @todo Parse/validate specific keys
     */
    public function setSite($site)
    {
        if (is_array($site)) {
            $this->site = $site;
        }
        else {
            die("ERROR: Not a valid site array.");
        }
    }


    /**
     * Get global site settings
     *
     */
    public function getSite()
    {
        return $this->site;
    }


    /**
     * Get publish command
     *
     */
    public function getPublishCommand()
    {
        return sprintf($this->publishParams["command"], $this->siteId);
    }


    /**
     * Get output folder
     *
     */
    public function getOutputFolder()
    {
        return sprintf($this->outputFolder, $this->siteId);
    }


    /**
     * Get publish clean command
     *
     */
    public function getPublishCleanCommand()
    {
        return sprintf($this->publishParams["cleanCommand"], $this->webRoot . "/" . $this->getOutputFolder());
    }


    /**
     * Get webRoot
     *
     */
    public function getWebRoot()
    {
        return $this->webRoot;
    }


    /**
     * Set search excludes
     *
     */
    public function setSearchExcludes($files, $dirs = [])
    {
        $this->searchParams["excludes"]["files"] = (is_array($files)) ? $files : [] ;
        $this->searchParams["excludes"]["dirs"] = (is_array($dirs)) ? $dirs : [] ;
    }


    /**
     * Get search excludes
     *
     */
    public function getSearchExcludes()
    {
        return $this->searchParams["excludes"];
    }


    /**
     * Get search file mask
     *
     */
    public function getSearchMask()
    {
        return $this->searchParams["fileMask"];
    }


    /**
     * Set publish excludes
     *
     */
    public function setPublishExcludes($dirs)
    {
        $this->publishParams["excludes"] = (is_array($dirs)) ? $dirs : [];
    }


    /**
     * Get publish excludes
     *
     */
    public function getPublishExcludes()
    {
        return $this->publishParams["excludes"];
    }


    /**
     * Get file paths
     *
     * @return array An array of needed paths and references
     */
    public function getPaths($file)
    {
        //$rootLabel = $GLOBALS["strings"]["Sitetree_Root_Label"];
        $rootLabel = $this->strings["Sitetree_Root_Label"];

        $result = [
            # with "./Section/Sub/File.md":
            "baseName" => basename($file, ".md"),  # "File"
            "sectionPath" => trim(dirname($file), "./"),  # "Section/Sub" (or "" for root)
            "sectionName" => "",  # "Section/Sub" (or "(root)" for root)
            "sectionId" => "",  # "#section-sub" (or "(root)" for root)
            "href" => ""  # "/Section/Sub/" (or "/" for root)
        ];

        # in root
        if ($result["sectionPath"] == "")
        {
            $result["sectionId"] = "#" . $this->getSectionId($rootLabel);
            $result["sectionName"] = $rootLabel;
            $result["href"] = "/";
        }
        else
        {
            $result["sectionId"] = "#" . $this->getSectionId($result["sectionPath"]);
            $result["sectionName"] = $result["sectionPath"];
            $result["href"] = "/" . $result["sectionPath"] . "/";
        }

        return $result;

    }


    /**
     * Get section id
     *
     * @return string A url-encoded id
     */
    public function getSectionId($name)
    {
        $toStrip = ["(",")"];
        $result = str_replace('/', "-", strtolower($name));
        return str_replace($toStrip, "", $result);
    }

}
