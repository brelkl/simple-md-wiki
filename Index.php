<?php

/**
 *  Index file for simple-md-wiki
 *  Copyright (C) 2016-2017  Brooks Kline <brookskline@caiyos.com>
 *
 *  This file is part of simple-md-wiki.
 *
 *  Simple-md-wiki is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Simple-md-wiki is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


// Process incoming GET data

// populate method and response object
$siteId = (isset($_GET["siteId"])) ? $_GET["siteId"] : null;
$location = (isset($_GET["location"])) ? $_GET["location"] : "/";

// fail when missing required params
if ( ! $siteId ) {
    die("<strong>ERROR: Site ID parameter missing; exiting.</strong><br/>siteId: $siteId");
}


// Load settings

// creates a 'siteObject' container will settings and methods
require_once("./Settings.php");

$currentSite = $siteObject->getSite();


// set name

if ( $location == "/" ) {
    $pageName = $siteObject->getString("Homepage_Name");
}
else {
    $pageName = substr($location, 0);
}

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<link href="/CSS/Default_Styles.css" rel="stylesheet" type="text/css"/>
<link href="/CSS/Pandoc_Code_Highlights.css" rel="stylesheet" type="text/css"/>
<?php echo $currentSite["header"]; ?>
<title>
<?php echo $currentSite["title"]; " - $location"; ?>
</title>
</head>

<div id="Branding">
<?php echo $currentSite["branding"]; ?>
</div>

<div id="Search" class="transition-basic">
  <input id="I_Search" type="text" autofocus placeholder="<?php echo $siteObject->getString("Search_Input_Label"); ?>"/>
  <button id="B_Search"><?php echo $siteObject->getString("Search_Button_Label"); ?></button>
</div>

<div id="Publish" class="transition-basic">
  <button id="B_Publish"><?php echo $siteObject->getString("Publish_Button_Label"); ?></button>
</div>

<div id="Search_Results" class="transition-basic">
  <h2><?php echo $siteObject->getString("Search_Results_Label"); ?></h2>
</div>

<div id="Content">
<?php

if ( $location == "/" ) {
    echo "<header><h1 class='title'>" . $siteObject->getString("Sitetree_Label") . "</h1></header>";
    if (file_exists($siteObject->getOutputFolder() . "/Sitetree.php")) {
        require_once($siteObject->getOutputFolder() . "/Sitetree.php");
    }
    else {
        echo $siteObject->getTemplate("No_Content_Published");
    }
}
else {
    require_once($siteObject->getOutputFolder() . $location . ".php");
}

?>

</div>

<div id="Footer">
  <p><a href="#"><?php echo $siteObject->getString("Top_Link_Label"); ?></a> | <a href="/"><?php echo $siteObject->getString("Sitetree_Label"); ?></a></p>
</div>

<!-- JS -->
<script src="/Third_Party/jquery-3.2.1.min.js" type="text/javascript">// JS Include</script>
<script src="/Javascript/Library.js" type="text/javascript">// JS Include</script>
<script type="text/javascript">
<?php echo "
    caiyosLib.globalVars.siteId = '$siteId';
    // strings
    caiyosLib.globalVars.strings.searchNoResults = '" . $siteObject->getTemplate("Search_Results_None") . "';
    caiyosLib.globalVars.params.topLinkElements = 'h1[class!=\"title\"],#Sitetree h2:not(:first)';
    caiyosLib.globalVars.params.topLinkHTML = '" . $siteObject->getTemplate("Top_Link") . "';\n"
;?>
</script>
<script src="/Javascript/Main.js" type="text/javascript">// JS Include</script>
</body>
</html>
