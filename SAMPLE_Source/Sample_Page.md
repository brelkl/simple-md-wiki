% Sample Markdown File
% Brooks Kline
% 2017-10-31


*Examples for simple-md-wiki.*


# Metadata blocks

Metadada will be included in the published files. You can include it, as needed, at the top of the documnet in the form:

```markdown
% Title
% Author
% Date
```

**Note** that simple-md-wiki expects a metadata title.


# Custom Code Tags

These are currently built into the stylesheet, and can be used in addition to the standard code highlighting provided by pandoc:

---------------------------------------

**address:** For displaying addresses (not standards-compliant):

```address
123 Number St.
Mathville USA
```

---------------------------------------

**ascii:** For ascii diagrams and such:

```ascii
+---------------+    +---------------+    +---------------+
|               |    |               |    |               |
|    Step 1:    |--->|    Step 2:    |--->|    Step 3:    |
|  Open Source  |    |               |    |    PROFIT!    |
|               |    |               |    |               |
+---------------+    +---------------+    +---------------+
```

---------------------------------------

**display:** This is used to display symbols:

```display
❶➁➌❹➄➏❼
```

---------------------------------------

**pre:** Standard preformatting style:

```pre
I have some notes here:
Note number:        1
Note number:        2
Etc. ...
```


# Images

Images have the following pre-defined styles:

  * `.border`
  * `.pad` (to add padding between the border and the image, i.e. for cropped images)

![Bat Boy, with a border.](/Images/Bat_Boy.jpg){ .border }
