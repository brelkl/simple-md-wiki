# simple-md-wiki

A simple tool to publish and serve markdown documents as html.


# Notes

* This was developed for my own internal use, and isn't (currently) built around security.

* My basic philosophy was to have a simple way to view and search my markdown notes in a browser.

* Tested on Ubuntu 16.04, 18.04.

* Simple-md-wiki uses pamdoc markdown syntax, and is written in php. It also utilizes pandoc and grep for publishing and searching.


# Installation

TODO Expand/clarify this.

1. Install required apps: nginx, php-fpm, pandoc.

2. Clone/download the simple-md-wiki repository to wherever you want to serve it from.

3. Use 'Nginx.conf.SAMPLE' and 'Settings.php.SAMPLE' to create an 'Nginx-SITE.conf' file and the 'Settings.php' file (where 'SITE' is your desired site name/id); update both files as needed.

4. Copy or symlink the 'Nginx-SITE.conf' file to `/etc/nginx/sites-enabled/`, and restart the nginx service.

5. Create the output folder in your web root (currently hard-coded as `Output-$siteId`, e.g. "Output-SAMPLE") and make it writable by the web server (www-data on Ubuntu).

6. Navigate to your site in a browser (as defined in Nginx-SITE.conf) and click the 'Publish' button.


# Pandoc Markdown Syntax

* See the pandoc man pages for full documentation (search for "PANDOC'S MARKDOWN").

* Take a look at `SAMPLE_Source/Sample.md` for a few examples.

