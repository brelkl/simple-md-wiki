/**
 *  Library JS file for simple-md-wiki
 *  Copyright (C) 2016-2017  Brooks Kline <brookskline@caiyos.com>
 *
 *  This file is part of simple-md-wiki.
 *
 *  Simple-md-wiki is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Simple-md-wiki is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


// Init lib object

let caiyosLib = {

  globalVars:
  {
    siteId: "",
    strings:
    {
      searchNoResults: "",
    },
    params:
    {
      topLinkElements: "",
      topLinkHTML: "",
    },
  },


  // Methods:

  /**
   * Init 'smooth scrolling'
   *
   * For internal (hash) links.
   *
   */
  initHashSmoothScrolling() {

    const $domRoot = $("html,body");

    $("a[href*='#']").click(function() {

      let hash = caiyosLib.jqSelectorEscape(this.hash);

      if (hash.length >= 1) {

        $domRoot.animate(
          {
            scrollTop: $(hash).offset().top,
          },
          400,
          function() {
            window.location.hash = hash;
          }
        );
        return false;
      }
      else {
        $domRoot.animate(
          {
            scrollTop: 0,
          }
        );
        return false;
      }
    });
  },


  /**
   * Escape a jquery selector
   *
   */
  jqSelectorEscape(selector) {
    const specialCharsRegex = /(:|\.|\[|\]|,|=|@|\!)/g;
    return selector.replace(specialCharsRegex, "\\$1" );
  },

};
