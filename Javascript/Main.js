/**
 *  Main JS file for simple-md-wiki
 *  Copyright (C) 2016-2017  Brooks Kline <brookskline@caiyos.com>
 *
 *  This file is part of simple-md-wiki.
 *
 *  Simple-md-wiki is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Simple-md-wiki is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */


// Declare local container object

let localWiki = {

  // Methods:

  /**
   * Init 'top links'
   *
   */
  initTopLinks() {
    $(caiyosLib.globalVars.params.topLinkElements)
      .before(caiyosLib.globalVars.params.topLinkHTML)
    ;
  },


  /**
   * Publish
   *
   */
  publishContent(siteId) {
    $.post(
      "/AJAX_Responder.php",
      {
        method: "Publish",
        siteId: siteId,
      },
      function (response) {
        if (response.status = 1) {
          // if success
          location.reload();
        }
        $("#Sitetree").html(response.data);
        $("#Publish").removeClass("active");
        localWiki.initTopLinks();
      },
      "json"
    );
  },


  /**
   * Search
   *
   */
  search(siteId) {
    const searchText = $("#I_Search").val();
    if (searchText) {
      $.post(
        "/AJAX_Responder.php",
        {
          method: "Search",
          siteId: siteId,
          searchText: searchText,
        },
        function (response) {
          $("#Search_Results").show();
          $("#Content").remove();
          if (response.status = 1) {
            $("#Search_Results").html(response.data);
          }
          else {
            $("#Search_Results").html(caiyosLib.globalVars.strings.searchNoResults);
          }
          caiyosLib.initHashSmoothScrolling();
        },
        "json"
      );
    }
  },

};


/**
 * $ initialization (dom-ready)
 *
 */
$(document).ready( function() {

  let name;

  // add pagename class id
  name = document.location.pathname.match(/[^\/]+$/);
  if (name) {
    $("body").addClass(name[0]);
  }
  else {
    $("body").addClass("root");
  }

  // init publish function
  $("#B_Publish").click(function() {
    $("#Publish").addClass("active");
    localWiki.publishContent(caiyosLib.globalVars.siteId);
  });

  // init search function
  $("#B_Search").click(function() {
    localWiki.search(caiyosLib.globalVars.siteId);
  });
    $("#I_Search").focus();

  if (! $("body").hasClass("root")) {
    localWiki.initTopLinks();
  }

  caiyosLib.initHashSmoothScrolling();

});


/**
 * $ initialization (window loaded)
 *
 */
$(window).on("load", function() {

  // smooth loading
  $("body").fadeIn(500);

  // bind search input field to button
  $("#I_Search").keypress(function(event) {
    let key = event.which;
    // the enter key code
    if (key == 13) {
      $("#B_Search").click();
    }
  });

  // navigate to url hash (if it exists)
  //let hash, name;
  let hash = caiyosLib.jqSelectorEscape(window.location.hash);
  //name = hash.substring(1);
  if (hash.length > 1) {
    $("html,body").animate(
      //{ scrollTop: $("*[id='" + name + "']").offset().top },
      {
        scrollTop: $(hash).offset().top,
      },
      400,
    );
  }

  // remove line numbers from code
  $(".sourceCode a.sourceLine").removeAttr('title');

});
